# 页面介绍
index：列表
detail：详情（包含删除逻辑）
add：新增
modify：修改

# TODO&bug
## 路由
- [x] 路由跳转不对，删除一个联系人，返回列表时应该刷新。但从浏览联系人详情的页面返回又不能刷新。需要强化路由，提供返回上一页并刷新上一页的方法。我看微信好像新增了一个EventChannel可以解决这类需求
- 修改页面修改完成后，应该回到详情页面，并刷新详情页面，同时关闭修改页面。非常需要返回到指定页，并将该页往上的所有栈都清空

## 列表
- pageSize是否可以做成uniList组件的一个属性？
- [x] reload应该是getList方法的参数吧，不应该是data里的内容吧。

## 数据校验
- [x] 新增和修改页面的数据校验。
- 希望在数据库的schema里，支持必填、正则等数据规则
- 然后前端和云函数都自动执行这个规则，不用到处写校验代码

## 权限验证
clientDB云函数里需要一套优雅的权限验证，和uni-id的权限结合

## 数据库
缺少字段：创建时间

## 搜索
需要补充

## ui库
### list
1. to属性语法提示不对，应该是urlstring
2. to属性必须搭配link使用？文档根本没提必须一起用。并且也不应该这么设计，没有link给个默认跳转方式就完事了
3. to里面的路径不对时，没有正确的错误提示。要正常提示xx页面不存在

### fab
[x] 一个默认的<uni-fab ></uni-fab>，怎么是黑色的？而且在左下角？要求默认在右下角，默认是蓝底的
fab默认要半透明啊
fab的大小怎么调节？并且目前默认大小也太大了
fab按钮也需要一个to属性，+号点击都是直接跳转到新建页面，还得写fabclick太麻烦了。
fab的中间那个+号，怎么改？比如要改成一个向上的箭头，就是常见的回到顶部，怎么做？例子要给出来。感觉应该是新增一个type
[x] 怎么fab一点，变这样了？没有子菜单时，它不能变啊！！！

### uform组件
缺少uform，类似element的表单组件
包括数据校验的轻重提示

### panel组件
补充一个组件，表单panel，每个panel包裹一批表单项，一个form可以有多个panel，panel之间有间隔。
panel里面的项目，都是图中那样，左边是title，右边是input或switch等表单项。
同样可以通过插槽扩展每一行的内容。

### link组件
缺少email、tel的类型