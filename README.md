## 简介

本项目展示了如何使用`uni-clientDB`，在客户端代码里完成对云数据库的增删改查。是开发者学习uniCloud的优秀参考示例。

`uni-clientDB`文档见：[https://uniapp.dcloud.net.cn/uniCloud/uni-clientDB](https://uniapp.dcloud.net.cn/uniCloud/uni-clientDB)

以在线通讯录为例，实现了如下功能：

- 通讯录列表，分页及下拉刷新
- 通讯录详情
- 通讯录添加
- 通讯录删除
- 通讯录修改
- 修改数据后刷新

即便仅参考列表到详情，本项目仍然是典型示例代码。

本项目发布了H5版，预览地址为：[https://static-7aa4d3db-b526-4fc4-9616-58433b94c7b2.bspapp.com](https://static-7aa4d3db-b526-4fc4-9616-58433b94c7b2.bspapp.com)

本项目未包含的问题，使用时需开发者自行补充相关代码：
- 云函数中删改数据的权限控制